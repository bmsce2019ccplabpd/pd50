#include<stdio.h>
#include<string.h>
struct employee
{
  int id;
  char name[30];
  int salary;
};
typedef struct employee Emp;
Emp input()
{
  Emp e;
  printf("Enter the name :");
  scanf("%s",e.name);
  printf("Enter the employee id:");
  scanf("%d",&e.id);
  printf("\nEnter the salary:");
  scanf("%d",&e.salary);
  return e;
}
void output(Emp e)
{
  printf("\nThe employee id is:%d",e.id);
  printf("\nThe employee name is:%s",e.name);
  printf("The salary of the employee is:%d",e.salary);
}
int main()
{
  Emp e;
  e=input();
  output(e);
  return 0;
}